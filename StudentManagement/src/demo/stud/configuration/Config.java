package demo.stud.configuration;

public interface Config {

	public String DRIVER_CLASS = "org.postgresql.Driver";
	public String URL = "jdbc:postgresql://localhost:5432/student_management";
	public String USER = "postgres";
	public String PASS = "Corp@123";
}
