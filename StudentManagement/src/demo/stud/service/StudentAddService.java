package demo.stud.service;

import java.util.Scanner;

import demo.stud.dao.StudentDao;
import demo.stud.model.Student;

public class StudentAddService {

	Scanner sc = new Scanner(System.in);
	Student st = new Student();

	public void addStudentService() {
		System.out.println("Enter Student ID");
		st.setId(sc.next());
		System.out.println("Enter Student Name");
		st.setName(sc.next());
		System.out.println("Enter Student Contact");
		st.setContact(sc.next());
		System.out.println("Enter Student Email");
		st.setEmail(sc.next());

		StudentDao dao = new StudentDao();
		boolean status = dao.addStudent(st);
		if (status) {
			System.out.println("Data Saved Successfully..............");
		} else {
			System.out.println("Data Not Saved Successfully..............");
		}
	}

}
