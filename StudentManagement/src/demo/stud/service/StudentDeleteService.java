package demo.stud.service;

import java.util.Scanner;

import demo.stud.dao.StudentDao;

public class StudentDeleteService {

	public void deleteStudent() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Student ID To Delete");
		String id = sc.next();
		StudentDao delete = new StudentDao();
		boolean status = delete.deleteStudent(id);
		if (status) {
			System.out.println("Student Deleted Successfully.......");
		} else {
			System.out.println("Student Not Deleted Successfully.......");
		}
	}
}
