package demo.stud.service;

import java.util.List;

import demo.stud.dao.StudentDao;
import demo.stud.model.Student;

public class StudentFetchService {

	public void studentFetchAll() {
		StudentDao fetch = new StudentDao();
		List<Student> st = fetch.fetchAllStudent();
		for(Student s : st) {
			System.out.println("=========================================================");
			System.out.println("                   ID: "+s.getId());
			System.out.println("                   NAME: "+s.getName());
			System.out.println("                   CONTACT: "+s.getContact());
			System.out.println("                   EMAIL: "+s.getEmail());
			System.out.println("=========================================================");
		}
	}
}
