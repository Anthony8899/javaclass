package demo.stud.service;

import java.util.Scanner;

import demo.stud.dao.StudentDao;
import demo.stud.model.Student;

public class StudentUpdateService {

	Scanner sc = new Scanner(System.in);
	Student st = new Student();

	public void updateStudentService() {
		System.out.println("Enter Student ID To Update");
		st.setId(sc.next());
		System.out.println("Enter Student Name");
		st.setName(sc.next());
		System.out.println("Enter Student Contact");
		st.setContact(sc.next());
		System.out.println("Enter Student Email");
		st.setEmail(sc.next());

		StudentDao dao = new StudentDao();
		boolean status = dao.updateStudent(st);
		if (status) {
			System.out.println("Data Updated Successfully..............");
		} else {
			System.out.println("Data Not Updated Successfully..............");
		}
	}
}
