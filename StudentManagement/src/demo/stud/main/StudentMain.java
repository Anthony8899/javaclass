package demo.stud.main;

import java.util.Scanner;

import demo.stud.service.StudentAddService;
import demo.stud.service.StudentDeleteService;
import demo.stud.service.StudentFetchService;
import demo.stud.service.StudentUpdateService;

public class StudentMain {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("----------- STUDENT MANAGEMENT ---------------");
		System.out.println("CHOOSE ANY OPTION......");
		System.out.println("1. INSERT\n2. FETCH ALL\n3. FETCH BY ID\n4. UPDATE\n5. DELETE\n6. EXIT");
		int opt = sc.nextInt();
		switch (opt) {
		case 1:
			StudentAddService add = new StudentAddService();
			add.addStudentService();
			main(null);
			break;
		case 2:
			StudentFetchService fetchAll = new StudentFetchService();
			fetchAll.studentFetchAll();
			main(null);
			break;
		case 3:
			System.out.println("Fetch By ID is not Implemented Yet");
			main(null);
			break;
		case 4:
			StudentUpdateService update = new StudentUpdateService();
			update.updateStudentService();
			main(null);
			break;
		case 5:
			StudentDeleteService delete = new StudentDeleteService();
			delete.deleteStudent();
			main(null);
			break;
		case 6:
			break;
		default:
			System.out.println("Invalid Option Please Try again!!!!!!!!!!");
			break;
		}

	}

}
