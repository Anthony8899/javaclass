package demo.stud.dao;

import static demo.stud.configuration.Config.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import demo.stud.model.Student;

public class StudentDao {

	Connection con = null;
	Statement st = null;
	PreparedStatement ps = null;
	ResultSet rs = null;

	public boolean addStudent(Student std) {
		try {
			Class.forName(DRIVER_CLASS);
			con = DriverManager.getConnection(URL, USER, PASS);
//			st = con.createStatement();
//			int i = st.executeUpdate("insert into student values " + "('" + std.getId() + "','" + std.getName() + "','"
//					+ std.getContact() + "','" + std.getEmail() + "')");

			ps = con.prepareStatement("insert into student values(?,?,?,?)");
			ps.setString(1, std.getId());
			ps.setString(2, std.getName());
			ps.setString(3, std.getContact());
			ps.setString(4, std.getEmail());
			int i = ps.executeUpdate();

			if (i > 0) {
				return true;
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return false;
	}

	public boolean updateStudent(Student std) {
		try {
			Class.forName(DRIVER_CLASS);
			con = DriverManager.getConnection(URL, USER, PASS);
			ps = con.prepareStatement("update student set name=?, contact=?, email=? where id=?");

			ps.setString(1, std.getName());
			ps.setString(2, std.getContact());
			ps.setString(3, std.getEmail());
			ps.setString(4, std.getId());
			int i = ps.executeUpdate();
			System.out.println("Updated Rows: " + i);

			if (i > 0) {
				return true;
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return false;
	}

	public List<Student> fetchAllStudent() {
		try {
			Class.forName(DRIVER_CLASS);
			List<Student> li = new ArrayList<>();
			Student st = null;
			con = DriverManager.getConnection(URL, USER, PASS);
			ps = con.prepareStatement("select * from student");
			rs = ps.executeQuery();
			while (rs.next()) {
				st = new Student();
				st.setId(rs.getString("id"));
				st.setName(rs.getString("name"));
				st.setContact(rs.getString("contact"));
				st.setEmail(rs.getString("email"));
				li.add(st);

			}
			if (!li.isEmpty()) {
				return li;
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return null;
	}

	public boolean deleteStudent(String id) {
		try {
			Class.forName(DRIVER_CLASS);
			con = DriverManager.getConnection(URL, USER, PASS);
			ps = con.prepareStatement("delete from student where id=?");
			ps.setString(1, id);
			int count = ps.executeUpdate();
			System.out.println("Deleted Rows: " + count);
			if (count > 0) {
				return true;
			}
		} catch (ClassNotFoundException e) {
			System.out.println(e);
		} catch (SQLException e) {
			System.out.println(e);
		}
		return false;

	}
}

/*
 * Insert, update, Delete - ExecuteUpdate Fetch - ExecuteQuery
 */
