package arrays;

import java.util.Scanner;

public class ArraysClass {

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the Terms: ");
		int n = sc.nextInt();
		int[] a = new int[n];

		for (int i = 0; i < a.length; i++) {
			System.out.print("Enter " + i + " number: ");
			a[i] = sc.nextInt();
		}

		System.out.println("The Given Numbers Are: ");
		for (int b : a) {
			System.out.println(b);
		}
	}

}
