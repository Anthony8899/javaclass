package arrays;

import java.util.Scanner;

public class Task1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Terms: ");
		int n = sc.nextInt();
		int[] num = new int[n];
		int total = 0;
		for (int i = 0; i < n; i++) {
			System.out.print("Enter " + i + " number: ");
			num[i] = sc.nextInt();
			total = total + num[i];
		}
		System.out.println("The sum of total numbers: " + total);
		System.out.println("The Average of given numbers: " + total / n);

	}

}
