package oops.enc;

public class Employee {

	private String name;
	private String age;
	private String con;
	private String email;
	private String salary;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getCon() {
		return con;
	}

	public void setCon(String con) {
		this.con = con;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSalary() {
		return salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

//	// setters
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	// Getters
//	public String getName() {
//		return name;
//	}
}
