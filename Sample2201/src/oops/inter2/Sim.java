package oops.inter2;

public interface Sim {
	
	public int age = 36;
	
	public void callService();

	public void msgService();

	public void dataService();
}
