package oops;

public class SumClass {

	int d;
	static int f = 10;

	SumClass(int d) {
		this.d = d;
		System.out.println("Constructor Calling");
	}

	public void sum(int a, int b) {
		int c = a + b + d;
		System.out.println("Sum Class: " + c);
	}

	public void sum(float a, float b) {
		System.out.println(a + b);
	}

	public void sum(double a, double b) {
		System.out.println(a + b);
	}

	public static void sum(int a, int b, int c) {
		int e = a + b + c;
		System.out.println("Sum Class: " + e);
	}

	static {
		System.out.println("Block Calling");
	}
}
