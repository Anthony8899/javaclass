package oops.abs;

public abstract class Parent {
	
	Parent(){
		System.out.println("This is Parent Cons");
	}

	public void sum() {
		System.out.println("This is Sum Impl");
	}

	public abstract void mul();

	public void div() {
		System.out.println("This is Div Impl");
	}

}
