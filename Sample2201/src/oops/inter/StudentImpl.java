package oops.inter;

public class StudentImpl implements Student {

	@Override
	public void addStudent() {
		System.out.println("Add Student Invoked");

	}

	@Override
	public void getStudent() {
		System.out.println("get Student Invoked");

	}

	@Override
	public void deleteStudent() {
		System.out.println("Delete Student Invoked");

	}

}
