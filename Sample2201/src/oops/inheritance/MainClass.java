package oops.inheritance;

public class MainClass {

	public static void main(String ars[]) {
		Child child = new Child();
		child.print();

//		GrandChild a = new GrandChild();
//		a.print2();
	}
}

class Parent {

	int property = 3;
	int car = 4;
	long amountInHand = 1000000;
	long amountInBank = 233333234;
	String car2 = "car";
}

class Child extends Parent {

	public int bus = 2;
	public int house = 3;

	public void print() {
		System.out.println(super.property);
		System.out.println(super.car);
		System.out.println(amountInHand);
		System.out.println(amountInBank);
		System.out.println(car2);

		System.out.println(bus);
		System.out.println(house);
	}
	
}

