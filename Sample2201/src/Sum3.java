import CustomException.UserNotFoundException;

public class Sum3 {

	public static int sum(int a, int b){
		try {
			return a / b;
		} catch (ArithmeticException e) {
			throw new UserNotFoundException("Error Occurred");
		}
	}
}
