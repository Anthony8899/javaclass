package loopingsprogram;

public class WhileLoop {

	public static void main(String[] args) {

		int i = 0;
		while (i < 0) {
			System.out.println(i);
			i++;
		}
		
		System.out.println("==============================================");

		int j = 0;
		do {
			System.out.println(j);
			j++;
		} while (j < 0);

	}

}

/**
 * While  - Entry COntrol loop
 * Do... while - Exit Contr
 * For 
 * foreach (Enhanced for loop)
 * 
 * 
 */
