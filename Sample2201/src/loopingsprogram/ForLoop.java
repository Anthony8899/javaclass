package loopingsprogram;

public class ForLoop {

	public static void main(String[] args) {

		for (String a : args) {
			System.out.println(a);
		}

//		for (int i = 0; i <= 100000; i++) {
//			System.out.println(i);
//		}

		int[] a = { 1, 2, 3, 4, 5, 6 };
		for (int b : a) {
			System.out.println(b);
		}

	}
}

/**
 * Class Object Inheritance Abstraction Encapsulation Polymorphism
 */