import CustomException.UserNotFoundException;

public class ExceptionClass {

	public static void main(String[] args) {
		int a = 1;
		int b = 0;
		try {
			int sum = Sum3.sum(a, b);
			System.out.println(sum);
		} catch (UserNotFoundException e) {
			System.out.println(e);
		} finally {
			System.out.println("Its finally block");
		}

		System.out.println("Its Done");
	}

}

/*
 * try catch() finally throw throws, userdefinedException
 */
