package sampleprograms;

class HelloWorld {

	static public void main(String[] args) {
		int dhoniHeight = 180;
		int viratHeight = 170;
//		int sewagHeight = 155;
		
//		System.out.println(!((dhoniHeight > viratHeight) && (viratHeight > sewagHeight))); //> or ==
		
		int status = (dhoniHeight < viratHeight) ? 345 : 3875;
		
		System.out.println(status);
	}

}

/**
 * int - whole numbers like 5,6,10000
 * long - 83945893459283983l
 * float - 10.5f, 12.687f
 * double - 10.1234342342342, 293847.24983712983749274398
 * boolean - true or false
 * byte - 0s and 1s
 * String - "asdfkasdlkfaslkfals"
 * char - 'a'
 * 
 * 
 * 
 * 
 * */
