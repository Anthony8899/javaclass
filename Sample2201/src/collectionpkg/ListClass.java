package collectionpkg;

import java.util.ArrayList;
import java.util.List;

public class ListClass {

	public static void main(String[] args) {
		
		List<String> li = new ArrayList<>();
		li.add("Test1");
		li.add("Test2");
		li.add("Test3");
		li.add("Test4");
		li.add("Test2");
		System.out.println(li);
		
		List<String> li2 = new ArrayList<String>();
		li2.add("Test1");
		li2.add("Test2");
		li2.add("Test3");
//		li2.add("Test4");
		
		System.out.println(li.equals(li2));
//		System.out.println(li.contains("Test4"));
//		System.out.println(li.get(2));
		System.out.println(li.hashCode());
		System.out.println(li.indexOf("Test2"));
		System.out.println(li.isEmpty());
		System.out.println(li.lastIndexOf("Test2"));
		System.out.println(li.remove(0));
		System.out.println(li.set(0, "addedd"));
		System.out.println(li.size());
		System.out.println(li.toArray());
		li.clear();
		System.out.println(li);
		

//		List<Student> list = new ArrayList<>();
//		Student s = new Student();
//		s.setName("Testing");
//		s.setAge(26);
//		s.setStudId(4584775845L);
//		Student s1 = new Student();
//		s1.setName("Testing1");
//		s1.setAge(22);
//		s1.setStudId(458442345L);
//		list.add(s);
//		list.add(s1);
//		List<Student> li2 = new ArrayList<>();
//		li2.addAll(list);
//		System.out.println(li2);
	}

}
