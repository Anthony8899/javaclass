package collectionpkg;

public class Student {

	private int age;
	private String name;
	private Long studId;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getStudId() {
		return studId;
	}

	public void setStudId(Long studId) {
		this.studId = studId;
	}

	@Override
	public String toString() {
		return "Student [age=" + age + ", name=" + name + ", studId=" + studId + "]";
	}
}
