package conditionalprograms;

public class SwitchClass {

	public static void main(String[] args) {

		char i = 'A';

		switch (i) {
		case 'a':
			System.out.println("This is Vowel");
			System.out.println("a for Apple");
			break;
		case 'e':
			System.out.println("This is Two");
			break;
		case 'i':
			System.out.println("This is THree");
			break;
		case 'o':
			System.out.println("This is Four");
			break;
		case 'u':
			System.out.println("This is Five");
			break;
		default:
			System.out.println("Invalid Value");

		}

	}

}
